package com.enterprisemath.articles.unitperformance;

import com.enterprisemath.utils.Dates;
import com.enterprisemath.utils.Month;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.time.DateUtils;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 * Performance test application for message broadcast module.
 *
 * @author radek.hecl
 */
public class MessageBroadcastModulePerformanceTestApp {

    /**
     * Prevents construction.
     */
    private MessageBroadcastModulePerformanceTestApp() {
    }

    /**
     * Main method.
     *
     * @param args arguments
     */
    public static void main(String args[]) {

        //
        // set up
        System.out.println("Setting up and generating test data");
        UserProvider userProvider = mock(UserProvider.class);
        BroadcastService broadcastService = new BroadcastService() {
            public void broadcastMessage(String userName, Date timestamp, String message) {
            }
        };
        MessageBroadcastModule module = MessageBroadcastModule.create(userProvider, broadcastService);

        System.out.println("Generating test data");
        when(userProvider.getUserName(any(String.class))).thenAnswer(new Answer<String>() {
            public String answer(InvocationOnMock invocation) throws Throwable {
                //Thread.sleep(10);
                String id = (String) invocation.getArguments()[0];
                return "user " + id;
            }
        });

        Date ts = Dates.createDate(2017, Month.JANUARY, 1);
        List<String> messages = new ArrayList<String>(1000000);
        for (int i = 0; i < 1000000; ++i) {
            int usid = i % 50;
            ts = DateUtils.addMilliseconds(ts, 1);
            messages.add("userId=" + usid + "|message=Hello world|timestamp=" + Dates.format(ts, "yyyy/MM/dd HH:mm:ss") +
                    "|ip=127.0.0.1|city=Brno|age=12|occupation=student");
        }
        System.out.println("Set up completed and data generated");

        //
        // wait to give user chance to connect profiler
        System.out.println("Timeout to allow attach profiler");
        try {
            for (int i = 0; i < 20; ++i) {
                Thread.sleep(1000);
                System.out.print(".");
            }
            System.out.println("");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Finished waiting for profiler");

        //
        // test
        System.out.println("Started performance test");
        long startTime = System.currentTimeMillis();
        for (String msg : messages) {
            module.processMessage(msg);
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Performance test finished");

        //
        // dump the result
        long duration = endTime - startTime;
        System.out.println("Num messages = " + messages.size());
        System.out.println("Duration = " + duration + "; referenceDuration = 14882");
        System.out.println("Duration / message = " + ((double) duration / messages.size()));
        System.out.println("JOB DONE!!!");
    }

}
