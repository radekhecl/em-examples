package com.enterprisemath.articles.unitperformance;

import com.enterprisemath.utils.Dates;
import com.enterprisemath.utils.Month;
import org.apache.commons.lang3.builder.ToStringBuilder;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Test case for message broadcast module.
 *
 * @author radek.hecl
 */
public class MessageBroadcastModuleTest {

    /**
     * Provider for user data.
     */
    private UserProvider userProvider;

    /**
     * Service for broadcast.
     */
    private BroadcastService broadcastService;

    /**
     * Tested module.
     */
    private MessageBroadcastModule module;

    /**
     * Creates new instance.
     */
    public MessageBroadcastModuleTest() {
    }

    /**
     * Sets up test environment.
     */
    @Before
    public void setUp() {
        userProvider = mock(UserProvider.class);
        broadcastService = mock(BroadcastService.class);

        module = MessageBroadcastModule.create(userProvider, broadcastService);
    }

    /**
     * Tests message processing.
     */
    @Test
    public void testProcessMessage() {
        when(userProvider.getUserName("1")).thenReturn("John Seaman");

        module.processMessage("userId=1|message=Hello world|timestamp=2017/01/01 12:00:00|ip=127.0.0.1|city=Brno|age=12|occupation=student");
        try {
            module.processMessage("message=Hello world|timestamp=2017/01/01 12:00:00|ip=127.0.0.1|city=Brno|age=12|occupation=student");
            fail("exception expected");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage(), e.getMessage().contains("wrong input message, user is missing"));
        }
        try {
            module.processMessage("userId=1|message=Hello world0|ip=127.0.0.1|city=Brno|age=12|occupation=student");
            fail("exception expected");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage(), e.getMessage().contains("wrong input message, timestamp is missing"));
        }
        try {
            module.processMessage("userId=1|timestamp=2017/01/01 12:00:00|ip=127.0.0.1|city=Brno|age=12|occupation=student");
            fail("exception expected");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage(), e.getMessage().contains("wrong input message, text is missing"));
        }

        verify(broadcastService).broadcastMessage("John Seaman", Dates.createTime(2017, Month.JANUARY, 1, 12, 0, 0), "Hello world");
        verifyNoMoreInteractions(broadcastService);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
