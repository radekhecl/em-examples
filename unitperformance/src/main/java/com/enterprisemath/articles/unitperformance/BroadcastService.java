package com.enterprisemath.articles.unitperformance;

import java.util.Date;

/**
 * Service for message broadcast.
 * 
 * @author radek.hecl
 */
public interface BroadcastService {

    /**
     * Broadcasts message.
     * 
     * @param userName user name
     * @param timestamp timestamp
     * @param message message
     */
    public void broadcastMessage(String userName, Date timestamp, String message);
}
