package com.enterprisemath.articles.unitperformance;

import com.enterprisemath.utils.Dates;
import com.enterprisemath.utils.ValidationUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Module for message broadcasting.
 *
 * @author radek.hecl
 */
public class MessageBroadcastModule {

    /**
     * Provider for user data.
     */
    private UserProvider userProvider;

    /**
     * Service for broadcast.
     */
    private BroadcastService broadcastService;

    /**
     * Cache for users.
     */
    private Map<String, String> usersCache = new HashMap<String, String>();

    /**
     * Creates new instance.
     */
    private MessageBroadcastModule() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        ValidationUtils.guardNotNull(userProvider, "userProvider cannot be null");
        ValidationUtils.guardNotNull(broadcastService, "broadcastService cannot be null");
    }

    /**
     * Processes message.
     *
     * @param message message
     */
    public void processMessage(String message) {
        String[] parts = message.split("\\|");
        String userName = null;
        String txt = null;
        Date now = null;
        for (String part : parts) {
            String[] subs = part.split("=", 2);
            if (subs[0].equals("userId")) {
                if (usersCache.containsKey(subs[1])) {
                    userName = usersCache.get(subs[1]);
                }
                else {
                    userName = userProvider.getUserName(subs[1]);
                    usersCache.put(subs[1], userName);
                }

            }
            else if (subs[0].equals("timestamp")) {
                now = Dates.parse(subs[1], "yyyy/MM/dd HH:mm:ss");
            }
            else if (subs[0].equals("message")) {
                txt = subs[1];
            }
        }
        ValidationUtils.guardNotNull(userName, "wrong input message, user is missing");
        ValidationUtils.guardNotNull(txt, "wrong input message, text is missing");
        ValidationUtils.guardNotNull(now, "wrong input message, timestamp is missing");
        broadcastService.broadcastMessage(userName, now, txt);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param userProvider provider for user data
     * @param broadcastService broadcast service
     * @return created object
     */
    public static MessageBroadcastModule create(UserProvider userProvider, BroadcastService broadcastService) {
        MessageBroadcastModule res = new MessageBroadcastModule();
        res.userProvider = userProvider;
        res.broadcastService = broadcastService;
        res.guardInvariants();
        return res;
    }
}
