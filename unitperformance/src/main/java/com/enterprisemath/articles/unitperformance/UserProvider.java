package com.enterprisemath.articles.unitperformance;

/**
 * Provider for user related data.
 * 
 * @author radek.hecl
 */
public interface UserProvider {

    /**
     * Returns user name.
     * 
     * @param userId user id
     * @return user name
     */
    public String getUserName(String userId);
}
